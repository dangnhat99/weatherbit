package cuoiki.nhom13.weatherforecast;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class WeatherApplication extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        WeatherApplication.context = getApplicationContext();

        Realm.init(this);
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
                .name("weather.realm").build());

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public static Context getAppContext() {
        return WeatherApplication.context;
    }
}
