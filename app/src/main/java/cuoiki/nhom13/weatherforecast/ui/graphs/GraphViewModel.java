package cuoiki.nhom13.weatherforecast.ui.graphs;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterWeatherModel;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowWeatherModel;
import io.realm.Realm;

public class GraphViewModel extends BaseViewModel {
    private Realm realm = Realm.getDefaultInstance();

    private MutableLiveData<TomorrowWeatherModel> _todayWeatherResponse = new MutableLiveData<>();
    public LiveData<TomorrowWeatherModel> tomorrowWeatherResponse = _todayWeatherResponse;

    private MutableLiveData<LaterWeatherModel> _laterWeatherResponse = new MutableLiveData<>();
    public LiveData<LaterWeatherModel> laterWeatherResponse = _laterWeatherResponse;

    public GraphViewModel(@NonNull Application application) {
        super(application);
    }

    //load data from localdatabase
    void getTodayWeatherFromRealm() {
        realm.where(TomorrowWeatherModel.class).findAllAsync().addChangeListener(tomorrowWeatherModels -> {
            if (tomorrowWeatherModels.size() > 0) {
                _todayWeatherResponse.setValue(tomorrowWeatherModels.get(0));
            }
        });
    }

    //load data from localdatabase
    void getLaterWeatherFromRealm() {
        realm.where(LaterWeatherModel.class).findAllAsync().addChangeListener(laterWeatherModels -> {
            if (laterWeatherModels.size() > 0) {
                _laterWeatherResponse.setValue(laterWeatherModels.get(0));
            }
        });
    }
}
