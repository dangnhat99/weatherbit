package cuoiki.nhom13.weatherforecast.ui.about;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.databinding.AboutFragmentBinding;

public class AboutFragment extends BaseFragment {
    AboutFragmentBinding binding;
    final static String ABOUT_TEXT ="<p style=\"text-align: center;\"><strong>B&Agrave;I TẬP CUỐI KỲ</strong></p>\n" +
            "<p style=\"text-align: center;\">C&Ocirc;NG NGHỆ DI ĐỘNG &nbsp;</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp;Đề t&agrave;i: <span style=\"background-color: rgb(247, 218, 100);\"><strong>Ứng dụng dự b&aacute;o thời tiết</strong></span></p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp;Sinh vi&ecirc;n thực hiện: &nbsp;+ Đặng Minh Nhật</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Cao Hữu Thuận</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Phan Văn Hoan</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Trần Nh&agrave;n</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp;Lớp: 17Nh14</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp;GVHD: Thầy Vũ</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp;Nguồn tham khảo:&nbsp;</p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp;+ API: <a href=\"https://www.weatherbit.io\">https://www.weatherbit.io</a></p>\n" +
            "<p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp;+ UI: <a href=\"https://github.com/martykan/forecastie\">https://github.com/martykan/forecastie</a></p>\n" +
            "<p style=\"text-align: left;\"><br></p>";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = AboutFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initView() {
        binding.txtAbout.setText(Html.fromHtml(ABOUT_TEXT));
    }

    @Override
    protected void initListener() {
        binding.toolbar.setNavigationOnClickListener(v -> {
            backFragment();
        });
    }
}
