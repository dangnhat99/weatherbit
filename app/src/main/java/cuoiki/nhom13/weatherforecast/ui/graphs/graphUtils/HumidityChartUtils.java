package cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;

import com.db.chart.Tools;
import com.db.chart.model.ChartSet;
import com.db.chart.model.LineSet;
import com.db.chart.view.ChartView;

import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;

public class HumidityChartUtils {
    public static void setHumidityChartbyHours(ChartView humidityChart, ArrayList<TomorrowDatum> list){

        LineSet dataset = new LineSet();

        for (int i = 0; i < list.size(); i++) {
            //chỉ lấy 48/8 số phần tử
            if (i % 8 == 0) {
                TomorrowDatum item = list.get(i);
                double temp = item.getRh();

                if (i%16 ==0) {
                    dataset.addPoint(
                            //get Date, sau đó biến đổi date ngắn gọn lại thành tháng-ngày:giờ
                            item.getDatetime().substring(item.getDatetime().length() - 8),
                            item.getRh().floatValue()
                    );
                } else {
                    dataset.addPoint(
                            "",
                            item.getRh().floatValue()
                    );
                }
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#3399FF"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int stepSize = 25;
        int min = 0;
        int max = 100;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        humidityChart.addData(data);
        humidityChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        humidityChart.setAxisBorderValues(min, max);
        humidityChart.setStep(stepSize);
        humidityChart.setLabelsColor(Color.parseColor("#000000"));
        humidityChart.setXAxis(false);
        humidityChart.setYAxis(false);
        humidityChart.setBorderSpacing(Tools.fromDpToPx(10));
        humidityChart.show();
    }

    public static void setHumidityChartbyDays(ChartView humidityChart, ArrayList<LaterDatum> list){

        LineSet dataset = new LineSet();

        for (int i = 1; i < list.size(); i++) {
            //chỉ lấy 1-6 (6 ngày tiếp theo)
            if (i  < 7) {
                LaterDatum item = list.get(i);
                double temp = item.getRh();

                dataset.addPoint(
                        //get Date, sau đó biến đổi date ngắn gọn lại thành ( tháng-ngày)
                        item.getDatetime().substring(item.getDatetime().length() - 5),
                        item.getRh().floatValue()
                );
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#3399FF"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int stepSize = 25;
        int min = 0;
        int max = 100;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        humidityChart.addData(data);
        humidityChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        humidityChart.setAxisBorderValues(min, max);
        humidityChart.setStep(stepSize);
        humidityChart.setLabelsColor(Color.parseColor("#000000"));
        humidityChart.setXAxis(false);
        humidityChart.setYAxis(false);
        humidityChart.setBorderSpacing(Tools.fromDpToPx(10));
        humidityChart.show();

    }


}

