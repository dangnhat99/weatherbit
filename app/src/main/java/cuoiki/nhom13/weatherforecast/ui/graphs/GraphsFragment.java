package cuoiki.nhom13.weatherforecast.ui.graphs;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProvider;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.pixplicity.easyprefs.library.Prefs;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefDefault;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;
import cuoiki.nhom13.weatherforecast.databinding.GraphsFragmentBinding;
import cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils.HumidityChartUtils;
import cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils.PressureChartUtils;
import cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils.TempChartUtils;
import cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils.WindChartUltis;

public class GraphsFragment extends BaseFragment {
    private GraphsFragmentBinding binding;
    private GraphViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = GraphsFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(GraphViewModel.class);

        binding.switchDay.setOn(Prefs.getBoolean(PrefKey.IS_GRAPH_16DAYS, PrefDefault.GRAPH_STATE));
        updateGraphs();
    }

    void updateGraphs() {
        if (!binding.switchDay.isOn()) {
            viewModel.getTodayWeatherFromRealm();
        } else {
            viewModel.getLaterWeatherFromRealm();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initListener() {
        binding.toolbar.setNavigationOnClickListener(v -> {
            backFragment();
        });

        binding.switchDay.setOnToggledListener((toggleableView, isOn) -> {
            Prefs.putBoolean(PrefKey.IS_GRAPH_16DAYS, isOn);
            updateGraphs();
        });

        viewModel.tomorrowWeatherResponse.observe(this, response -> {
            TempChartUtils.setTempChartbyHours(binding.graphTemperature, new ArrayList<>(response.getData()));
            //todo
            PressureChartUtils.setPressureChartbyHours(binding.graphPressure, new ArrayList<>(response.getData()));
            WindChartUltis.setWindChartbyHours(binding.graphWindspeed, new ArrayList<>(response.getData()));
        });
        viewModel.laterWeatherResponse.observe(this, response -> {
            TempChartUtils.setTempChartbyDays(binding.graphTemperature, new ArrayList<>(response.getData()));
            //todo
            PressureChartUtils.setPressureChartbyDays(binding.graphPressure, new ArrayList<>(response.getData()));
            WindChartUltis.setWindChartbyDays(binding.graphWindspeed, new ArrayList<>(response.getData()));
        });
        viewModel.tomorrowWeatherResponse.observe(this, response -> {
            HumidityChartUtils.setHumidityChartbyHours(binding.graphHumidity, new ArrayList<>(response.getData()));
            //todo
        });
        viewModel.laterWeatherResponse.observe(this, response -> {
            HumidityChartUtils.setHumidityChartbyDays(binding.graphHumidity, new ArrayList<>(response.getData()));
            //todo
        });
    }
}
