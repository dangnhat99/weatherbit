package cuoiki.nhom13.weatherforecast.ui.map;

public enum MapSheetState {
    CLOUDS, RAIN, WIND, TEMPERATURE
}
