package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.tomorrow;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Date;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;


import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;
import cuoiki.nhom13.weatherforecast.databinding.WeatherFragmentBinding;
import cuoiki.nhom13.weatherforecast.utils.event.RefreshEvent;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefDefault;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;

public class TomorrowFragment extends BaseFragment {
    private WeatherFragmentBinding binding;
    TomorrowAdapter adapter;
    TomorrowViewModel viewModel;
    ArrayList<TomorrowDatum> tomorrowList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = WeatherFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    void loadApi() {
        viewModel.getTomorrowWeatherbyLatLon(Prefs.getDouble(PrefKey.CITY_LAT, PrefDefault.CITY_LAT),Prefs.getDouble(PrefKey.CITY_LON, PrefDefault.CITY_LON));
        binding.loadingView.setVisibility(View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
        if (event instanceof RefreshEvent) {
            Log.d("TOMORROW", "onMessageEvent: refresh");
            if (((RefreshEvent) event).isApiRefresh) {
//                viewModel.getTomorrowWeatherbyLatLon(Prefs.getDouble(PrefKey.CITY_LAT, PrefDefault.CITY_LAT),Prefs.getDouble(PrefKey.CITY_LON, PrefDefault.CITY_LON));
                loadApi();
                Log.d("TOMORROW", "onMessageEvent: refresh");
            } else {
                //todo
                //loadDataBase Realm
                viewModel.getTomorrowWeatherFromRealm();
                Log.d("TOMORROW", "onMessageEvent: database");
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TOMORROW", "eventbus register");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.d("TOMORROW", "eventbus unregister");
    }

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(TomorrowViewModel.class);

        adapter = new TomorrowAdapter(tomorrowList, getContext());
        binding.recyclerviewWeather.setAdapter(adapter);
        binding.recyclerviewWeather.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    @Override
    protected void initListener() {
        viewModel.tomorrowWeatherResponse.observe(this,TomorrowWeatherModel->{
            tomorrowList.clear();
            binding.loadingView.setVisibility(View.INVISIBLE);
            for(TomorrowDatum i:TomorrowWeatherModel.getData()){

                Calendar now = Calendar.getInstance();
                Calendar then = Calendar.getInstance();

                now.add(Calendar.DAY_OF_YEAR, 1);
                then.setTime(new Date((i.getTs().intValue()*1000L)));

                if (now.get(Calendar.YEAR) == then.get(Calendar.YEAR) && now.get(Calendar.DAY_OF_YEAR) == then.get(Calendar.DAY_OF_YEAR)){
                    tomorrowList.add(i);
                }
            }
            adapter.notifyDataSetChanged();
        });

}
}