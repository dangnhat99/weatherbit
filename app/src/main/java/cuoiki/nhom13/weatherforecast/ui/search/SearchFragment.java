package cuoiki.nhom13.weatherforecast.ui.search;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentDatum;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;
import cuoiki.nhom13.weatherforecast.databinding.SearchFragmentBinding;
import cuoiki.nhom13.weatherforecast.utils.CustomLoadingDialog;
import cuoiki.nhom13.weatherforecast.utils.event.ChooseCityEvent;

public class SearchFragment extends BaseFragment implements SearchCityAdapter.SearchAdapterCallBack {
    SearchFragmentBinding binding;
    SearchViewModel viewModel;
    SearchCityAdapter adapter;
    private CustomLoadingDialog loadingDialog;
    ArrayList<CurrentDatum> currentDatas = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = SearchFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
        if (event instanceof FindCityEvent) {
            Log.d("Search", "onMessageEvent");
            loadingDialog.show();
            viewModel.getCurrentWeatherByCity(((FindCityEvent) event).city);
            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    ;

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        loadingDialog = new CustomLoadingDialog(getActivity());
        adapter = new SearchCityAdapter(currentDatas, getContext(), this);
        binding.rvCity.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvCity.setAdapter(adapter);
    }

    @Override
    protected void initListener() {
        viewModel.currentResponse.observe(this, results -> {
            currentDatas.clear();
            currentDatas.addAll(results);
            adapter.notifyDataSetChanged();
            loadingDialog.dismiss();
        });

        viewModel.errorString.observe(this, error -> {
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            loadingDialog.dismiss();
            navController.popBackStack();
        });

        binding.toolbar.setNavigationOnClickListener(v -> {
            navController.popBackStack();
        });

        binding.toolbar.setTitle(R.string.list_of_cities);
    }

    @Override
    public void onItemClicked(int position) {
        CurrentDatum item = currentDatas.get(position);

        if (item != null) {
            Prefs.putDouble(PrefKey.CITY_LAT, item.getLat());
            Prefs.putDouble(PrefKey.CITY_LON, item.getLon());
            EventBus.getDefault().postSticky(new ChooseCityEvent());
            navController.popBackStack();
        }
    }
}
