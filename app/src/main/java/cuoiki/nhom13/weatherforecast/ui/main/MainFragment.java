package cuoiki.nhom13.weatherforecast.ui.main;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.appbar.AppBarLayout;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentDatum;
import cuoiki.nhom13.weatherforecast.databinding.MainFragmentBinding;
import cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.later.LaterFragment;
import cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.today.TodayFragment;
import cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.tomorrow.TomorrowFragment;
import cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.ViewPagerAdapter;
import cuoiki.nhom13.weatherforecast.ui.search.FindCityEvent;
import cuoiki.nhom13.weatherforecast.utils.CustomLoadingDialog;
import cuoiki.nhom13.weatherforecast.utils.EditTextDialog;
import cuoiki.nhom13.weatherforecast.utils.UnitConverter;
import cuoiki.nhom13.weatherforecast.utils.event.ChooseCityEvent;
import cuoiki.nhom13.weatherforecast.utils.event.RefreshEvent;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefDefault;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;

public class MainFragment extends BaseFragment implements LocationListener {
    public final static long REFRESHING_INTERVAL = 2 * 60 * 1000;
    protected static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 1;
    //5 minutes
    public static boolean isSwitchFragment = false;

    private MainFragmentBinding binding;
    private MainViewModel viewModel;
    private ViewPagerAdapter viewPagerAdapter;
    private PopupMenu mainMenu;
    private EditTextDialog findCityDialog;
    private CustomLoadingDialog loadingDialog;

    private final TodayFragment todayFragment = new TodayFragment();
    private final TomorrowFragment tomorrowFragment = new TomorrowFragment();
    private final LaterFragment laterFragment = new LaterFragment();

    private LocationManager locationManager;
    ProgressDialog progressDialog;

    boolean isAppOpen = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    void updateLocationByGPS() {
        progressDialog = new ProgressDialog(getContext());

        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showLocationSettingsDialog();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_ACCESS_FINE_LOCATION);
            }

        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage(getString(R.string.getting_location));
            progressDialog.setCancelable(false);
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        locationManager.removeUpdates(MainFragment.this);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }
            });
            progressDialog.show();
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        } else {
            showLocationSettingsDialog();
        }
    }

    private void showLocationSettingsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(R.string.location_settings);
        alertDialog.setMessage(R.string.location_settings_message);
        alertDialog.setPositiveButton(R.string.location_settings_button, (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        });
        alertDialog.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        viewPagerAdapter.addFragment(todayFragment, getString(R.string.today));
        viewPagerAdapter.addFragment(tomorrowFragment, getString(R.string.tomorrow));
        viewPagerAdapter.addFragment(laterFragment, getString(R.string.later));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(Object event) {
        if (event instanceof ChooseCityEvent) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                Prefs.putLong(PrefKey.LAST_UPDATE, 0);
                Log.d("MAIN", "eventbus register");
                loadCurrentWeather();
                EventBus.getDefault().removeStickyEvent(event);
            }, 20);
        }
    }

    ;

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setCurrentItem(0, false);


        mainMenu = new PopupMenu(getContext(), binding.btnMenu);
        mainMenu.getMenuInflater().inflate(R.menu.menu_main, mainMenu.getMenu());

        findCityDialog = new EditTextDialog(getContext(), getString(R.string.enter_the_city_name_you_want_to_find), getString(R.string.search_for_city), getString(R.string.ok), getString(R.string.cancel));
        loadingDialog = new CustomLoadingDialog(getActivity());

        if (!isSwitchFragment) {
            new Handler(Looper.getMainLooper()).postDelayed(this::loadCurrentWeather, 200);
            isSwitchFragment = false;
        }
    }

    @Override
    protected void initListener() {
        viewModel.currentWeatherResponse.observe(this, currentWeatherModel -> {
            isAppOpen = true;
            binding.swipeRefreshLayout.setRefreshing(false);
            //update today weather
            CurrentDatum currentWeather = currentWeatherModel.getData().get(0);
            binding.txtCity.setText(currentWeather.getCityName());
            binding.todayTemperature.setText(String.format(getString(R.string.temp_C_format), currentWeather.getTemp()));
            binding.todayDescription.setText(currentWeather.getCurrentWeather().getDescription());
            binding.todayWind.setText(String.format(getString(R.string.wind_ms_format), currentWeather.getWindSpd()));
            binding.todayPressure.setText(String.format(getString(R.string.pressure_mb_format), currentWeather.getPres()));
            binding.todayHumidity.setText(String.format(getString(R.string.humidity_format), currentWeather.getRh()));
            binding.todaySunrise.setText(String.format(getString(R.string.sunrise_format), currentWeather.getSunrise()));
            binding.todaySunset.setText(String.format(getString(R.string.sunset_format), currentWeather.getSunset()));
            binding.todayUvIndex.setText(
                    String.format(getString(R.string.uv_format),
                            currentWeather.getUv(),
                            UnitConverter.UVtoRiskText(currentWeather.getUv()
                            )));
            binding.todayIcon.setText(
                    UnitConverter.getWeatherIconAsText(
                            currentWeather.getCurrentWeather().getCode(),
                            currentWeather.getPod().equals("d")
                    )
            );

            //save last update time in Prefs
            if (loadingDialog.getDialog() != null && loadingDialog.getDialog().isShowing()) {
                long lastUpdateTime = Calendar.getInstance().getTimeInMillis();
                Prefs.putLong(PrefKey.LAST_UPDATE, lastUpdateTime);
            }

            binding.lastUpdate.setText(
                    String.format(getString(R.string.lastupdate_format),
                            UnitConverter.formatTimeWithDayIfNotToday(Prefs.getLong(PrefKey.LAST_UPDATE, 0)))
            );

            loadingDialog.dismiss();
        });

        viewModel.errorString.observe(this, error -> {
            isAppOpen = true;
            loadingDialog.dismiss();
            binding.swipeRefreshLayout.setRefreshing(false);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
            viewModel.getCurrentWeatherFromRealm();
            EventBus.getDefault().post(new RefreshEvent(false));
        });

        binding.appBarLayout.addOnOffsetChangedListener((AppBarLayout.BaseOnOffsetChangedListener) (appBarLayout, i) -> {
            // Only allow pull to refresh when scrolled to top
            binding.swipeRefreshLayout.setEnabled(i == 0);
        });

        findCityDialog.setOnEditTextDialogListener(new EditTextDialog.OnEditTextDialogListener() {
            @Override
            public void onAccept(String editText) {
                if (!editText.trim().isEmpty()) {
                    EventBus.getDefault().postSticky(new FindCityEvent(editText));

                    changeFragment(R.id.mainFragment_to_searchFragment);
                }
            }

            @Override
            public void onCancel() {
            }
        });

        mainMenu.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_map:
                    changeFragment(R.id.mainFragment_to_mapFragment);
                    break;
                case R.id.action_about:
                    changeFragment(R.id.mainFragment_to_aboutFragment);
                    break;
                case R.id.action_graphs:
                    changeFragment(R.id.mainFragment_to_graphFragment);
                    break;
//                case R.id.action_settings:
//                    changeFragment(R.id.mainFragment_to_settingFragment);
//                    break;
                case R.id.action_gps:
                    updateLocationByGPS();
                    break;
            }
            return false;
        });

        binding.btnMenu.setOnClickListener(v -> {
            mainMenu.show();
        });

        binding.btnSearch.setOnClickListener(v -> {
            findCityDialog.show();
        });

        binding.btnRefresh.setOnClickListener(v -> {
            loadCurrentWeather();
        });

        binding.swipeRefreshLayout.setOnRefreshListener(this::loadCurrentWeather);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("MAIN", "onViewCreated");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("MAIN", "onDestroyView");
    }

    void loadCurrentWeather() {
        //is app is instantly opened, load data
        if (Calendar.getInstance().getTimeInMillis() - Prefs.getLong(PrefKey.LAST_UPDATE, 0) > REFRESHING_INTERVAL) {
            //if app already opened, check is last update time > 2 minutes then refresh data
            loadingDialog.show();
            viewModel.getCurrentWeatherbyLatLon(
                    Prefs.getDouble(PrefKey.CITY_LAT, PrefDefault.CITY_LAT),
                    Prefs.getDouble(PrefKey.CITY_LON, PrefDefault.CITY_LON)
            );
            //post event to update in fragments of viewpager
            EventBus.getDefault().post(new RefreshEvent(true));
        } else {
            binding.swipeRefreshLayout.setRefreshing(false);
            if (isAppOpen) {
                Toast.makeText(getContext(), getString(R.string.only_allow_refresh_data_after_2_minutes), Toast.LENGTH_SHORT).show();
            }
            viewModel.getCurrentWeatherFromRealm();
            //post event to update in fragments of viewpager
            EventBus.getDefault().post(new RefreshEvent(false));
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        progressDialog.hide();
        try {
            locationManager.removeUpdates(this);
        } catch (SecurityException e) {
            Log.e("LocationManager", "Error while trying to stop listening for location updates. This is probably a permissions issue", e);
        }
        Log.i("LOCATION (" + location.getProvider().toUpperCase() + ")", location.getLatitude() + ", " + location.getLongitude());
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        viewModel.getCurrentWeatherbyLatLon(latitude, longitude);
        EventBus.getDefault().post(new RefreshEvent(true));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }
}
