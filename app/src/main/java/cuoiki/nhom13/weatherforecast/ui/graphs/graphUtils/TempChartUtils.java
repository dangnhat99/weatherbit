package cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.db.chart.Tools;
import com.db.chart.model.ChartSet;
import com.db.chart.model.LineSet;
import com.db.chart.view.ChartView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;

public class TempChartUtils {
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void setTempChartbyHours(ChartView tempChart, ArrayList<TomorrowDatum> list) {
        float minTemp = 1000;
        float maxTemp = -1000;

        LineSet dataset = new LineSet();

        for (int i = 0; i < list.size(); i++) {
            //chỉ lấy 48/8 số phần tử
            if (i % 8 == 0) {
                TomorrowDatum item = list.get(i);
                double temp = item.getTemp();
                //bài toán tìm max, min
                minTemp = (float) Math.min(Math.floor(temp), minTemp);
                maxTemp = (float) Math.max(Math.floor(temp), maxTemp);

                if (i%16 ==0) {
                    dataset.addPoint(
                            //get Date, sau đó biến đổi date ngắn gọn lại thành tháng-ngày:giờ
                            item.getDatetime().substring(item.getDatetime().length() - 8),
                            item.getTemp().floatValue()
                    );
                } else {
                    dataset.addPoint(
                            "",
                            item.getTemp().floatValue()
                    );
                }
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#FF5722"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minTemp + (maxTemp - minTemp) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxTemp - minTemp) / 4));
        int min = middle - 2 * stepSize;
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        tempChart.addData(data);
        tempChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        tempChart.setAxisBorderValues(min, max);
        tempChart.setStep(stepSize);
        tempChart.setLabelsColor(Color.parseColor("#000000"));
        tempChart.setXAxis(false);
        tempChart.setYAxis(false);
        tempChart.setBorderSpacing(Tools.fromDpToPx(10));
        tempChart.show();
    }

    public static void setTempChartbyDays(ChartView tempChart, ArrayList<LaterDatum> list) {
        float minTemp = 1000;
        float maxTemp = -1000;

        LineSet dataset = new LineSet();

        for (int i = 1; i < list.size(); i++) {
            //chỉ lấy 1-6 (6 ngày tiếp theo)
            if (i  < 7) {
                LaterDatum item = list.get(i);
                double temp = item.getTemp();
                //bài toán tìm max, min
                minTemp = (float) Math.min(Math.floor(temp), minTemp);
                maxTemp = (float) Math.max(Math.floor(temp), maxTemp);

                    dataset.addPoint(
                            //get Date, sau đó biến đổi date ngắn gọn lại thành ( tháng-ngày)
                            item.getDatetime().substring(item.getDatetime().length() - 5),
                            item.getTemp().floatValue()
                    );
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#FF5722"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minTemp + (maxTemp - minTemp) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxTemp - minTemp) / 4));
        int min = middle - 2 * stepSize;
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        tempChart.addData(data);
        tempChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        tempChart.setAxisBorderValues(min, max);
        tempChart.setStep(stepSize);
        tempChart.setLabelsColor(Color.parseColor("#000000"));
        tempChart.setXAxis(false);
        tempChart.setYAxis(false);
        tempChart.setBorderSpacing(Tools.fromDpToPx(10));
        tempChart.show();
    }
}
