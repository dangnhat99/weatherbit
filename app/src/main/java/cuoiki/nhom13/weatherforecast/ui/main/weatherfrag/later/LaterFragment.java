package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.later;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.databinding.WeatherFragmentBinding;
import cuoiki.nhom13.weatherforecast.utils.event.RefreshEvent;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefDefault;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;

public class LaterFragment extends BaseFragment {
    private WeatherFragmentBinding binding;
    private LaterViewModel viewModel;
    private ArrayList<LaterDatum> listLaterWeather = new ArrayList<>();
    LaterWeatherAdapter adapter;

    public LaterFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("LATER", "eventbus register");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
        if (event instanceof RefreshEvent) {
            if (((RefreshEvent) event).isApiRefresh) {
                loadLaterWeather();
            } else {
                loadLaterWeatherFromRealm();
            }
        }
    }

    ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = WeatherFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(LaterViewModel.class);

        adapter = new LaterWeatherAdapter(listLaterWeather, getContext());

        RecyclerView rv = binding.recyclerviewWeather;
        binding.recyclerviewWeather.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerviewWeather.setAdapter(adapter);
    }

    @Override
    protected void initListener() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 12);
        Calendar then = Calendar.getInstance();


        final long millisInDay = 86400 * 1000;

        viewModel.laterWeatherResponse.observe(this, laterWeatherModel -> {
            listLaterWeather.clear();
            binding.loadingView.setVisibility(View.INVISIBLE);
            for (LaterDatum weather : laterWeatherModel.getData()) {
                then.setTime(new Date(weather.getTs() * 1000L));
                then.set(Calendar.HOUR_OF_DAY, 12);
                long dateDiff = then.getTimeInMillis() / millisInDay - today.getTimeInMillis() / millisInDay;
                if (dateDiff == 0 || dateDiff == 1) continue;
                ;
                listLaterWeather.add(weather);
            }
            adapter.notifyDataSetChanged();
        });
    }

    void loadLaterWeather() {
        binding.loadingView.setVisibility(View.VISIBLE);
        viewModel.getLaterWeatherbyLatLon(Prefs.getDouble(PrefKey.CITY_LAT, PrefDefault.CITY_LAT), Prefs.getDouble(PrefKey.CITY_LON, PrefDefault.CITY_LON));
    }

    void loadLaterWeatherFromRealm() {
        binding.loadingView.setVisibility(View.VISIBLE);
        viewModel.getLaterWeatherFromRealm();
    }
}