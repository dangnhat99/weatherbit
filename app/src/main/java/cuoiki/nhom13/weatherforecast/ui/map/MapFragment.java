package cuoiki.nhom13.weatherforecast.ui.map;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pixplicity.easyprefs.library.Prefs;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;
import cuoiki.nhom13.weatherforecast.databinding.MapFragmentBinding;

public class MapFragment extends BaseFragment {
    final static int MAP_DEFAULT_ZOOM = 7;
    final static String OpenWeather_API_KEY = "3e29e62e2ddf6dd3d2ebd28aed069215";
    final static String MAP_MAIN_URL_FORMAT = "file:///android_asset/map.html?lat=%f&lon=%f&appid=%s&zoom=%d";
    final static String MAP_CLOUD_URL = "javascript:map.removeLayer(rainLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);map.addLayer(cloudsLayer);";
    final static String MAP_RAIN_URL = "javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(tempLayer);map.addLayer(rainLayer);";
    final static String MAP_WIND_URL = "javascript:map.removeLayer(cloudsLayer);map.removeLayer(rainLayer);map.removeLayer(tempLayer);map.addLayer(windLayer);";
    final static String MAP_TEMP_URL = "javascript:map.removeLayer(cloudsLayer);map.removeLayer(windLayer);map.removeLayer(rainLayer);map.addLayer(tempLayer);";

    MapFragmentBinding binding;
    MapSheetState mapSheetState = MapSheetState.CLOUDS;
    double lat, lon = 0;
    int zoom = MAP_DEFAULT_ZOOM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = MapFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface", "DefaultLocale"})
    @Override
    protected void initView() {

        lat = Prefs.getDouble(PrefKey.CITY_LAT, 0);
        lon = Prefs.getDouble(PrefKey.CITY_LON, 0);

        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.loadUrl(String.format(
                MAP_MAIN_URL_FORMAT,
                lat,
                lon,
                OpenWeather_API_KEY,
                MAP_DEFAULT_ZOOM
        ));
        binding.webView.addJavascriptInterface(new HybridInterface(), "NativeInterface");
    }

    private void setMapState() {
        switch (mapSheetState) {
            case RAIN: {
                binding.webView.loadUrl(MAP_RAIN_URL);
                break;
            }
            case WIND: {
                binding.webView.loadUrl(MAP_WIND_URL);
                break;
            }
            case CLOUDS: {
                binding.webView.loadUrl(MAP_CLOUD_URL);
                break;
            }
            case TEMPERATURE: {
                binding.webView.loadUrl(MAP_TEMP_URL);
                break;
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void initListener() {
        binding.toolbar.setNavigationOnClickListener(v -> {
            backFragment();
        });

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setMapState();
            }
        });

        binding.navigationBar.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.map_clouds: {
                    mapSheetState = MapSheetState.CLOUDS;
                    setMapState();
                    break;
                }
                case R.id.map_rain: {
                    mapSheetState = MapSheetState.RAIN;
                    setMapState();
                    break;
                }
                case R.id.map_temperature: {
                    mapSheetState = MapSheetState.TEMPERATURE;
                    setMapState();
                    break;
                }
                case R.id.map_wind: {
                    mapSheetState = MapSheetState.WIND;
                    setMapState();
                    break;
                }
            }
            return true;
        });
    }

    class HybridInterface {
        @JavascriptInterface
        public void transferLatLon(double lat, double lon) {
            MapFragment.this.lat = lat;
            MapFragment.this.lon = lon;
        }

        @JavascriptInterface
        public void transferZoom(int level) {
            MapFragment.this.zoom = level;
        }
    }
}
