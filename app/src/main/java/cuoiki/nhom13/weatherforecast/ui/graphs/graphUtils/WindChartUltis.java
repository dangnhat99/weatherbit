package cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;

import com.db.chart.Tools;
import com.db.chart.model.ChartSet;
import com.db.chart.model.LineSet;
import com.db.chart.view.ChartView;

import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;

public class WindChartUltis {
    public static void setWindChartbyHours(ChartView windChart, ArrayList<TomorrowDatum> list) {
        float minWindSpd = 1000;
        float maxWindSpd = -1000;

        LineSet dataset = new LineSet();

        for (int i = 0; i < list.size(); i++) {
            //chỉ lấy 48/8 số phần tử
            if (i % 8 == 0) {
                TomorrowDatum item = list.get(i);
                double windSpd = item.getWindSpd();
                //bài toán tìm max, min
                minWindSpd = (float) Math.min(Math.floor(windSpd), minWindSpd);
                maxWindSpd = (float) Math.max(Math.floor(windSpd), maxWindSpd);

                if (i%16 ==0) {
                    dataset.addPoint(
                            //get Date, sau đó biến đổi date ngắn gọn lại thành tháng-ngày:giờ
                            item.getDatetime().substring(item.getDatetime().length() - 8),
                            item.getWindSpd().floatValue()
                    );
                } else {
                    dataset.addPoint(
                            "",
                            item.getWindSpd().floatValue()
                    );
                }
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#57FF22"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minWindSpd + (maxWindSpd - minWindSpd) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxWindSpd - minWindSpd) / 4));
        int min = Math.max(middle - 2 * stepSize, 0);
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        windChart.addData(data);
        windChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        windChart.setAxisBorderValues(min, max);
        windChart.setStep(stepSize);
        windChart.setLabelsColor(Color.parseColor("#000000"));
        windChart.setXAxis(false);
        windChart.setYAxis(false);
        windChart.setBorderSpacing(Tools.fromDpToPx(10));
        windChart.show();
    }

    public static void setWindChartbyDays(ChartView windChart, ArrayList<LaterDatum> list) {
        float minWindSpd = 1000;
        float maxWindSpd = -1000;

        LineSet dataset = new LineSet();

        for (int i = 1; i < list.size(); i++) {
            //chỉ lấy 1-6 (6 ngày tiếp theo)
            if (i  < 7) {
                LaterDatum item = list.get(i);
                double windSpd = item.getWindSpd();
                //bài toán tìm max, min
                minWindSpd = (float) Math.min(Math.floor(windSpd), minWindSpd);
                maxWindSpd = (float) Math.max(Math.floor(windSpd), maxWindSpd);

                dataset.addPoint(
                        //get Date, sau đó biến đổi date ngắn gọn lại thành ( tháng-ngày)
                        item.getDatetime().substring(item.getDatetime().length() - 5),
                        item.getWindSpd().floatValue()
                );
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#57FF22"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minWindSpd + (maxWindSpd - minWindSpd) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxWindSpd - minWindSpd) / 4));
        int min =  Math.max(middle - 2 * stepSize, 0);
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        windChart.addData(data);
        windChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        windChart.setAxisBorderValues(min, max);
        windChart.setStep(stepSize);
        windChart.setLabelsColor(Color.parseColor("#000000"));
        windChart.setXAxis(false);
        windChart.setYAxis(false);
        windChart.setBorderSpacing(Tools.fromDpToPx(10));
        windChart.show();
    }
}

