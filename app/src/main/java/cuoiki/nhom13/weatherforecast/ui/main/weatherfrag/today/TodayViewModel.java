package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.today;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowWeatherModel;
import cuoiki.nhom13.weatherforecast.data.online.ApiConstants;
import cuoiki.nhom13.weatherforecast.data.online.ApiService;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.realm.Realm;

public class TodayViewModel extends BaseViewModel {

    private ApiService apiService = ApiService.getInstance();
    private Realm realm = Realm.getDefaultInstance();
    private MutableLiveData<TomorrowWeatherModel> _todayWeatherResponse = new MutableLiveData<>();
    public LiveData<TomorrowWeatherModel> tomorrowWeatherResponse = _todayWeatherResponse;

    private MutableLiveData<String> _errorLiveData = new MutableLiveData<>();
    public LiveData<String> errorString = _errorLiveData;

    public TodayViewModel(@NonNull Application application) {
        super(application);
    }

    public void getTodayWeatherbyLatLon(Double lat, Double lon) {
        apiService.getService().getTomorrowWeatherByLatLon(ApiConstants.API_KEY, lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<TomorrowWeatherModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull TomorrowWeatherModel tomorrowWeatherModel) {
                        _todayWeatherResponse.setValue(tomorrowWeatherModel);
                        realm.executeTransactionAsync(realm -> {
                            realm.delete(TomorrowWeatherModel.class);
                            realm.insert(tomorrowWeatherModel);
                        });
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        _errorLiveData.setValue(e.getLocalizedMessage());
                    }
                });
    }

    //load data from localdatabase
    void getTodayWeatherFromRealm() {
        realm.where(TomorrowWeatherModel.class).findAllAsync().addChangeListener(tomorrowWeatherModels -> {
            if (tomorrowWeatherModels.size() > 0) {
                _todayWeatherResponse.setValue(tomorrowWeatherModels.get(0));
            }
        });
    }
}

