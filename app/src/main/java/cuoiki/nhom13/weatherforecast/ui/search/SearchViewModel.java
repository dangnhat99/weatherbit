package cuoiki.nhom13.weatherforecast.ui.search;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentDatum;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentWeatherModel;
import cuoiki.nhom13.weatherforecast.data.online.ApiConstants;
import cuoiki.nhom13.weatherforecast.data.online.ApiService;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class SearchViewModel extends BaseViewModel {
    private final ApiService apiService = ApiService.getInstance();

    private MutableLiveData<List<CurrentDatum>> _currentResponse = new MutableLiveData<>();
    public LiveData<List<CurrentDatum>> currentResponse = _currentResponse;

    private MutableLiveData<String> _errorLiveData = new MutableLiveData<>();
    public LiveData<String> errorString = _errorLiveData;

    public SearchViewModel(@NonNull Application application) {
        super(application);
    }

    void getCurrentWeatherByCity(String city) {
        apiService.getService().getCurrentWeatherByCity(ApiConstants.API_KEY, city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CurrentWeatherModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull CurrentWeatherModel currentWeatherModel) {
                        if (currentWeatherModel.getData().size() > 0) {
                            _currentResponse.setValue(currentWeatherModel.getData());
                        } else {
                            _errorLiveData.setValue(getApplication().getString(R.string.no_result_found));
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        if (e.getMessage() == null ) {
                            _errorLiveData.setValue(getApplication().getString(R.string.no_result_found));
                        } else {
                            _errorLiveData.setValue(getApplication().getString(R.string.connection_error));
                        }
                    }
                });
    }
}
