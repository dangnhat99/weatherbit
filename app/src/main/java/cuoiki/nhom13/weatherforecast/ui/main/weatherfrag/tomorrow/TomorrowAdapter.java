package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.tomorrow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;
import cuoiki.nhom13.weatherforecast.databinding.WeatherItemBinding;
import cuoiki.nhom13.weatherforecast.utils.UnitConverter;

public class TomorrowAdapter extends RecyclerView.Adapter<TomorrowAdapter.TomorrowViewHolder> {

    public TomorrowAdapter(ArrayList<TomorrowDatum> tomorrowWeatherList, Context context) {
        this.tomorrowWeatherList = tomorrowWeatherList;
        this.context = context;

    }

    List<TomorrowDatum> tomorrowWeatherList;
    Context context;

    @NonNull
    @Override
    public TomorrowAdapter.TomorrowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.weather_item, parent, false);
        return new TomorrowAdapter.TomorrowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TomorrowAdapter.TomorrowViewHolder holder, int position) {
        holder.bindView(tomorrowWeatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return tomorrowWeatherList.size();
    }

    public class TomorrowViewHolder extends RecyclerView.ViewHolder {
        WeatherItemBinding binding;
        public TomorrowViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = WeatherItemBinding.bind(itemView);
        }


        @SuppressLint("SetTextI18n")
        public void bindView(TomorrowDatum item) {
            String dateString = "";
            try {
                SimpleDateFormat simpleDateFormat= new SimpleDateFormat("EEEE YYYY.MM.dd - HH:mm" );
                simpleDateFormat.setTimeZone(TimeZone.getDefault());
                dateString = simpleDateFormat.format(item.getTs()*1000L);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

            binding.itemDate.setText(dateString);
            binding.itemDescription.setText(item.getWeather().getDescription());
            binding.itemWind.setText(String.format(context.getString(R.string.wind_ms_format),item.getWindSpd()));
            binding.itemPressure.setText(String.format(context.getString(R.string.pressure_mb_format),item.getPres()));
            binding.itemHumidity.setText(String.format(context.getString(R.string.humidity_format),item.getRh()));
            binding.itemIcon.setText(UnitConverter.getWeatherIconAsText(item.getWeather().getCode().intValue(),item.getPod().equals("d")));
            binding.itemTemperature.setText(String.format(context.getString(R.string.temp_C_format),item.getTemp()));
        }
    }
}
