package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.today;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;
import cuoiki.nhom13.weatherforecast.databinding.WeatherFragmentBinding;
import cuoiki.nhom13.weatherforecast.utils.event.RefreshEvent;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefDefault;
import cuoiki.nhom13.weatherforecast.data.offline.pref.PrefKey;

public class TodayFragment extends BaseFragment {
    private WeatherFragmentBinding binding;
    TodayAdapter adapter;
    TodayViewModel viewModel;

    @NonNull
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        return super.getDefaultViewModelProviderFactory();
    }

    ArrayList<TomorrowDatum> todayList = new ArrayList<>();

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TODAY", "eventbus register");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Object event) {
       if (event instanceof RefreshEvent) {
           if (((RefreshEvent) event).isApiRefresh) {
               loadApi();
           } else {
               //todo
               //load realm database
               viewModel.getTodayWeatherFromRealm();
           }
       }
    };

    void loadApi() {
        viewModel.getTodayWeatherbyLatLon(Prefs.getDouble(PrefKey.CITY_LAT, PrefDefault.CITY_LAT),Prefs.getDouble(PrefKey.CITY_LON, PrefDefault.CITY_LON));
        binding.loadingView.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = WeatherFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initView() {
        viewModel = new ViewModelProvider(this).get(TodayViewModel.class);

        adapter = new TodayAdapter(todayList, getContext());
        binding.recyclerviewWeather.setAdapter(adapter);
        binding.recyclerviewWeather.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    protected void initListener() {
        viewModel.tomorrowWeatherResponse.observe(this,TomorrowWeatherModel->{
            todayList.clear();
            binding.loadingView.setVisibility(View.INVISIBLE);
            try {
                for(TomorrowDatum i:TomorrowWeatherModel.getData()){
                    // Tạo một đối tượng Date mô tả thời điểm hiện tại.
                    Date now = new Date();
                    Date then = new Date((i.getTs().intValue()*1000L));
                    if (then.getDay() - now.getDay() ==0){
                        todayList.add(i);
                    }
                }
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }
}
