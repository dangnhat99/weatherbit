package cuoiki.nhom13.weatherforecast.ui.setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cuoiki.nhom13.weatherforecast.base.BaseFragment;
import cuoiki.nhom13.weatherforecast.databinding.SettingFragmentBinding;

public class SettingFragment extends BaseFragment {
    private SettingFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = SettingFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }
}
