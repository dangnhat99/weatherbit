package cuoiki.nhom13.weatherforecast.ui.main;

import android.os.Bundle;

import cuoiki.nhom13.weatherforecast.base.BaseActivity;
import cuoiki.nhom13.weatherforecast.databinding.MainActivityBinding;

public class MainActivity extends BaseActivity {
    private MainActivityBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = MainActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainFragment.isSwitchFragment = false;
    }
}