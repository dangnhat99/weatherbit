package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.later;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterWeatherModel;
import cuoiki.nhom13.weatherforecast.data.online.ApiConstants;
import cuoiki.nhom13.weatherforecast.data.online.ApiService;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.realm.Realm;

public class LaterViewModel extends BaseViewModel {
    private ApiService apiService = ApiService.getInstance();
    private Realm realm = Realm.getDefaultInstance();

    private MutableLiveData<LaterWeatherModel> _laterWeatherResponse = new MutableLiveData<>();
    public LiveData<LaterWeatherModel> laterWeatherResponse = _laterWeatherResponse;

    private MutableLiveData<String> _errorLiveData = new MutableLiveData<>();
    public LiveData<String> errorString = _errorLiveData;

    public LaterViewModel(@NonNull Application application) {
        super(application);
    }

    void getLaterWeatherbyLatLon(Double lat, Double lon) {
        apiService.getService().getLaterWeatherByLatLon(ApiConstants.API_KEY, lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LaterWeatherModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull LaterWeatherModel laterWeatherModel) {
                        if (laterWeatherModel.getData().size()>0) {
                            _laterWeatherResponse.setValue(laterWeatherModel);

                            //store new data to realm database
                            realm.executeTransactionAsync(realm -> {
                                realm.delete(LaterWeatherModel.class);
                                realm.insert(laterWeatherModel);
                            });
                        } else {
                            _errorLiveData.setValue(getApplication().getString(R.string.no_data_found));
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        _errorLiveData.setValue(e.getLocalizedMessage());
                    }
                });
    }

    //load data from localdatabase
    void getLaterWeatherFromRealm() {
        realm.where(LaterWeatherModel.class).findAllAsync().addChangeListener(laterWeatherModels -> {
            if (laterWeatherModels.size() > 0) {
                _laterWeatherResponse.setValue(laterWeatherModels.get(0));
            }
        });
    }
}
