package cuoiki.nhom13.weatherforecast.ui.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentDatum;
import cuoiki.nhom13.weatherforecast.databinding.FindCityItemBinding;
import cuoiki.nhom13.weatherforecast.utils.UnitConverter;

public class SearchCityAdapter extends RecyclerView.Adapter<SearchCityAdapter.CityViewHolder> {
    List<CurrentDatum> cityList;
    Context context;
    SearchAdapterCallBack callBack;

    public SearchCityAdapter(List<CurrentDatum> cityList, Context context, SearchAdapterCallBack callBack) {
        this.cityList = cityList;
        this.context = context;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.find_city_item, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        holder.bindView();
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    interface SearchAdapterCallBack {
        void onItemClicked(int position);
    }

    class CityViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        FindCityItemBinding binding;
        GoogleMap map;

        public CityViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = FindCityItemBinding.bind(itemView);
            binding.mapView.onCreate(null);

            binding.getRoot().setOnClickListener(v -> {
                callBack.onItemClicked(getAdapterPosition());
            });
        }

        void bindView() {
            CurrentDatum item = cityList.get(getAdapterPosition());

            binding.txtCityName.setText(item.getCityName());
            binding.txtDescribtion.setText(item.getCurrentWeather().getDescription());
            binding.txtTemp.setText(context.getString(R.string.temp_C_format, item.getTemp()));
            binding.txtIcon.setText(UnitConverter.getWeatherIconAsText(
                    item.getCurrentWeather().getCode(),
                    item.getPod().equals("d")
            ));
            binding.mapView.getMapAsync(this);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            map = googleMap;

            if (map == null) return;
            LatLng location = new LatLng(cityList.get(getAdapterPosition()).getLat(), cityList.get(getAdapterPosition()).getLon());
            UiSettings settings = map.getUiSettings();
            settings.setZoomControlsEnabled(true);

            map.addMarker(new MarkerOptions().position(location));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 8f));
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }
}
