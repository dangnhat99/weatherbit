package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.later;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.databinding.WeatherItemBinding;
import cuoiki.nhom13.weatherforecast.utils.UnitConverter;

public class LaterWeatherAdapter extends RecyclerView.Adapter<LaterWeatherAdapter.LaterViewHolder> {

    ArrayList<LaterDatum> listLaterWeather;
    Context context;

    LaterWeatherAdapter(ArrayList<LaterDatum> listLaterWeathher, Context context) {
        this.listLaterWeather = listLaterWeathher;
        this.context = context;
    }

    @NonNull
    @Override
    public LaterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_item, parent, false);
        return new LaterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LaterViewHolder holder, int position) {
        LaterDatum laterWeather = listLaterWeather.get(position);
        holder.bind(laterWeather);
    }

    @Override
    public int getItemCount() {
        return listLaterWeather.size();
    }

    class LaterViewHolder extends RecyclerView.ViewHolder {

        WeatherItemBinding binding;

        public LaterViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = WeatherItemBinding.bind(itemView);
        }

        public void bind(LaterDatum laterWeather) {
            String dateString = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE YYYY.MM.dd");
            dateString = simpleDateFormat.format(laterWeather.getTs()*1000L);

            binding.itemDate.setText(dateString);
            binding.itemDescription.setText(laterWeather.getLaterWeather().getDescription());
            binding.itemHumidity.setText(String.format(context.getString(R.string.humidity_format), laterWeather.getRh()));
            binding.itemPressure.setText(String.format(context.getString(R.string.pressure_mb_format), laterWeather.getPres()));
            binding.itemTemperature.setText(String.format(context.getString(R.string.temp_C_format), laterWeather.getTemp()));
            binding.itemWind.setText(String.format(context.getString(R.string.wind_ms_format), laterWeather.getWindSpd()));
            binding.itemIcon.setText(UnitConverter.getWeatherIconAsText(laterWeather.getLaterWeather().getCode().intValue(), true));
        }
    }
}
