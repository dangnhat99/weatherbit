package cuoiki.nhom13.weatherforecast.ui.main;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentWeatherModel;
import cuoiki.nhom13.weatherforecast.data.online.ApiConstants;
import cuoiki.nhom13.weatherforecast.data.online.ApiService;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.realm.Realm;

public class MainViewModel extends BaseViewModel {
    private ApiService apiService = ApiService.getInstance();
    private Realm realm = Realm.getDefaultInstance();

    private MutableLiveData<CurrentWeatherModel> _currentWeatherResponse = new MutableLiveData<>();
    public LiveData<CurrentWeatherModel> currentWeatherResponse = _currentWeatherResponse;

    private MutableLiveData<String> _errorLiveData = new MutableLiveData<>();
    public LiveData<String> errorString = _errorLiveData;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    void getCurrentWeatherbyLatLon(Double lat, Double lon) {
        apiService.getService().getCurrentWeatherByLatLon(ApiConstants.API_KEY, lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CurrentWeatherModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);

                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull CurrentWeatherModel currentWeatherModel) {
                        if (currentWeatherModel.getData().size()>0) {
                            _currentWeatherResponse.setValue(currentWeatherModel);

                            //store new data to realm database
                            realm.executeTransactionAsync(realm -> {
                                realm.delete(CurrentWeatherModel.class);
                                realm.insert(currentWeatherModel);
                            });
                        } else {
                            _errorLiveData.setValue(getApplication().getString(R.string.no_data_found));
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        _errorLiveData.setValue(e.getLocalizedMessage());
                    }
                });
    }

    //load data from localdatabase
    void getCurrentWeatherFromRealm() {
        realm.where(CurrentWeatherModel.class).findAllAsync().addChangeListener(currentWeatherModels -> {
            if (currentWeatherModels.size() > 0) {
                _currentWeatherResponse.setValue(currentWeatherModels.get(0));
            }
        });
    }
}
