package cuoiki.nhom13.weatherforecast.ui.main.weatherfrag.tomorrow;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cuoiki.nhom13.weatherforecast.base.BaseViewModel;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowWeatherModel;
import cuoiki.nhom13.weatherforecast.data.online.ApiConstants;
import cuoiki.nhom13.weatherforecast.data.online.ApiService;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.realm.Realm;

public class TomorrowViewModel extends BaseViewModel {

    private Realm realm = Realm.getDefaultInstance();
    private ApiService apiService = ApiService.getInstance();

    private MutableLiveData<TomorrowWeatherModel> _tomorrowWeatherResponse = new MutableLiveData<>();
    public LiveData<TomorrowWeatherModel> tomorrowWeatherResponse = _tomorrowWeatherResponse;

    private MutableLiveData<String> _errorLiveData = new MutableLiveData<>();
    public LiveData<String> errorString = _errorLiveData;

    public TomorrowViewModel(@NonNull Application application) {
        super(application);
    }

    public void getTomorrowWeatherbyLatLon(Double lat, Double lon) {
        apiService.getService().getTomorrowWeatherByLatLon(ApiConstants.API_KEY, lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<TomorrowWeatherModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull TomorrowWeatherModel tomorrowWeatherModel) {
                        _tomorrowWeatherResponse.setValue(tomorrowWeatherModel);
                        //store new data to realm database
                        realm.executeTransactionAsync(realm -> {
                            realm.delete(TomorrowWeatherModel.class);
                            realm.insert(tomorrowWeatherModel);
                        });
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        e.printStackTrace();
                        _errorLiveData.setValue(e.getLocalizedMessage());
                    }
                });
    }
    //load data from localdatabase
    void getTomorrowWeatherFromRealm() {
        realm.where(TomorrowWeatherModel.class).findAllAsync().addChangeListener(tomorrowWeatherModels -> {
            if (tomorrowWeatherModels.size() > 0) {
                _tomorrowWeatherResponse.setValue(tomorrowWeatherModels.get(0));
            }
        });
    }
}
