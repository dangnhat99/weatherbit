package cuoiki.nhom13.weatherforecast.ui.graphs.graphUtils;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;

import com.db.chart.Tools;
import com.db.chart.model.ChartSet;
import com.db.chart.model.LineSet;
import com.db.chart.view.ChartView;

import java.util.ArrayList;

import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterDatum;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowDatum;

public class PressureChartUtils {
    public static void setPressureChartbyHours(ChartView presChart, ArrayList<TomorrowDatum> list) {
        float minPres = 1000;
        float maxPres = -1000;

        LineSet dataset = new LineSet();

        for (int i = 0; i < list.size(); i++) {
            //chỉ lấy 48/8 số phần tử
            if (i % 8 == 0) {
                TomorrowDatum item = list.get(i);
                double pres = item.getPres();
                //bài toán tìm max, min
                minPres = (float) Math.min(Math.floor(pres), minPres);
                maxPres = (float) Math.max(Math.floor(pres), maxPres);

                if (i%16 ==0) {
                    dataset.addPoint(
                            //get Date, sau đó biến đổi date ngắn gọn lại thành tháng-ngày:giờ
                            item.getDatetime().substring(item.getDatetime().length() - 8),
                            item.getPres().floatValue()
                    );
                } else {
                    dataset.addPoint(
                            "",
                            item.getPres().floatValue()
                    );
                }
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#ffda22"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minPres + (maxPres - minPres) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxPres - minPres) / 4));
        int min = Math.max(middle - 2 * stepSize, 0);
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        presChart.addData(data);
        presChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        presChart.setAxisBorderValues(min, max);
        presChart.setStep(stepSize);
        presChart.setLabelsColor(Color.parseColor("#000000"));
        presChart.setXAxis(false);
        presChart.setYAxis(false);
        presChart.setBorderSpacing(Tools.fromDpToPx(10));
        presChart.show();
    }

    public static void setPressureChartbyDays(ChartView presChart, ArrayList<LaterDatum> list) {
        float minPres = 1000;
        float maxPres = -1000;

        LineSet dataset = new LineSet();

        for (int i = 1; i < list.size(); i++) {
            //chỉ lấy 1-6 (6 ngày tiếp theo)
            if (i  < 7) {
                LaterDatum item = list.get(i);
                double pres = item.getPres();
                //bài toán tìm max, min
                minPres = (float) Math.min(Math.floor(pres), minPres);
                maxPres = (float) Math.max(Math.floor(pres), maxPres);

                dataset.addPoint(
                        //get Date, sau đó biến đổi date ngắn gọn lại thành ( tháng-ngày)
                        item.getDatetime().substring(item.getDatetime().length() - 5),
                        item.getPres().floatValue()
                );
            }
        }

        dataset.setSmooth(false);
        //màu đỏ (ghi hex color tuỳ ý, nhưng nhớ điền đủ 6 số sau # cả crash)
        dataset.setColor(Color.parseColor("#ffda22"));
        dataset.setThickness(4);

        //tìm step cho cột y
        int middle = Math.round(minPres + (maxPres - minPres) / 2);
        int stepSize = Math.max(1, (int) Math.ceil(Math.abs(maxPres - minPres) / 4));
        int min =  Math.max(middle - 2 * stepSize, 0);
        int max = middle + 2 * stepSize;

        ArrayList<ChartSet> data = new ArrayList<>();
        data.add(dataset);
        presChart.addData(data);
        presChart.setGrid(ChartView.GridType.HORIZONTAL, 4, 1, new Paint() {{
            setStyle(Paint.Style.STROKE);
            setAntiAlias(true);
            setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
            setStrokeWidth(1);
        }});
        presChart.setAxisBorderValues(min, max);
        presChart.setStep(stepSize);
        presChart.setLabelsColor(Color.parseColor("#000000"));
        presChart.setXAxis(false);
        presChart.setYAxis(false);
        presChart.setBorderSpacing(Tools.fromDpToPx(10));
        presChart.show();
    }
}
