package cuoiki.nhom13.weatherforecast.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;

import cuoiki.nhom13.weatherforecast.R;

public class CustomLoadingDialog {

    Activity activity;
    Dialog dialog;
    public CustomLoadingDialog(Activity activity) {
        this.activity = activity;
    }

    public void show() {

        dialog  = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_loading_view);

        ImageView gifImageView = dialog.findViewById(R.id.custom_loading_imageView);
        DrawableImageViewTarget imageViewTarget = new DrawableImageViewTarget(gifImageView);

//        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);
        Glide.with(activity)
                .load(R.drawable.loading)
                .placeholder(R.drawable.loading)
                .centerCrop()
                .transition(
                        new DrawableTransitionOptions().crossFade()
                )
                .into(imageViewTarget);

        dialog.show();
    }

    public void dismiss(){
        if (dialog  !=null) {
            dialog.dismiss();
        }
    }

    public Dialog getDialog() {
        return dialog;
    }
}