package cuoiki.nhom13.weatherforecast.utils.event;

public class RefreshEvent {
    public boolean isApiRefresh;

    public RefreshEvent(boolean isApiRefresh) {
        this.isApiRefresh = isApiRefresh;
    }
}
