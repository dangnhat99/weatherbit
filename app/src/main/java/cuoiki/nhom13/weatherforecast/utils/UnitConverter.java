package cuoiki.nhom13.weatherforecast.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import cuoiki.nhom13.weatherforecast.R;
import cuoiki.nhom13.weatherforecast.WeatherApplication;

public class UnitConverter {

    public static String formatTimeWithDayIfNotToday(long timeInMillis) {
        Calendar now = Calendar.getInstance();
        Calendar lastCheckedCal = new GregorianCalendar();
        lastCheckedCal.setTimeInMillis(timeInMillis);
        Date lastCheckedDate = new Date(timeInMillis);
        String timeFormat = android.text.format.DateFormat.getTimeFormat(WeatherApplication.getAppContext()).format(lastCheckedDate);
        if (now.get(Calendar.YEAR) == lastCheckedCal.get(Calendar.YEAR) &&
                now.get(Calendar.DAY_OF_YEAR) == lastCheckedCal.get(Calendar.DAY_OF_YEAR)) {
            // Same day, only show time
            return timeFormat;
        } else {
            return android.text.format.DateFormat.getDateFormat(WeatherApplication.getAppContext()).format(lastCheckedDate) + " " + timeFormat;
        }
    }

    public static String UVtoRiskText(double value) {
        /* based on: https://en.wikipedia.org/wiki/Ultraviolet_index */
        if (value >= 0.0 && value < 3.0) {
            return WeatherApplication.getAppContext().getString(R.string.uvi_low);
        } else if (value >= 3.0 && value < 6.0) {
            return WeatherApplication.getAppContext().getString(R.string.uvi_moderate);
        } else if (value >= 6.0 && value < 8.0) {
            return WeatherApplication.getAppContext().getString(R.string.uvi_high);
        } else if (value >= 8.0 && value < 11.0) {
            return WeatherApplication.getAppContext().getString(R.string.uvi_very_high);
        } else if (value >= 11.0 && value <= 14.0) {
            return WeatherApplication.getAppContext().getString(R.string.uvi_extreme);
        } else {
            return WeatherApplication.getAppContext().getString(R.string.uvi_no_info);
        }
    }

    public static String getWeatherIconAsText(int weatherId, boolean isDay) {
        int id = weatherId / 100;
        String icon = "";

        if (id == 2) {
            // thunderstorm
            switch (weatherId) {
                case 210:
                case 211:
                case 212:
                case 221:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_lightning);
                    break;
                case 200:
                case 201:
                case 202:
                case 230:
                case 231:
                case 232:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_thunderstorm);
                    break;
            }
        } else if (id == 3) {
            // drizzle/sprinkle
            switch (weatherId) {
                case 302:
                case 311:
                case 312:
                case 314:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_rain);
                    break;
                case 310:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_rain_mix);
                    break;
                case 313:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_showers);
                    break;
                case 300:
                case 301:
                case 321:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_sprinkle);
                    break;
            }
        } else if (id == 5) {
            // rain
            switch (weatherId) {
                case 500:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_sprinkle);
                    break;
                case 511:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_rain_mix);
                    break;
                case 520:
                case 521:
                case 522:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_showers);
                    break;
                case 531:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_storm_showers);
                    break;
                case 501:
                case 502:
                case 503:
                case 504:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_rain);
                    break;
            }
        } else if (id == 6) {
            // snow
            switch (weatherId) {
                case 611:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_sleet);
                    break;
                case 612:
                case 613:
                case 615:
                case 616:
                case 620:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_rain_mix);
                    break;
                case 600:
                case 601:
                case 602:
                case 621:
                case 622:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_snow);
                    break;
            }
        } else if (id == 7) {
            // atmosphere
            switch (weatherId) {
                case 711:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_smoke);
                    break;
                case 721:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_day_haze);
                    break;
                case 731:
                case 761:
                case 762:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_dust);
                    break;
                case 751:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_sandstorm);
                    break;
                case 771:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_cloudy_gusts);
                    break;
                case 781:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_tornado);
                    break;
                case 701:
                case 741:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_fog);
                    break;
            }
        } else if (id == 8) {
            // clear sky or cloudy
            switch (weatherId) {
                case 800:
                    icon = isDay
                            ? WeatherApplication.getAppContext().getString(R.string.weather_day_sunny)
                            : WeatherApplication.getAppContext().getString(R.string.weather_night_clear);
                    break;
                case 801:
                case 802:
                    icon = isDay
                            ? WeatherApplication.getAppContext().getString(R.string.weather_day_cloudy)
                            : WeatherApplication.getAppContext().getString(R.string.weather_night_alt_cloudy);
                    break;
                case 803:
                case 804:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_cloudy);
                    break;
            }
        } else if (id == 9) {
            switch (weatherId) {
                case 900:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_tornado);
                    break;
                case 901:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_storm_showers);
                    break;
                case 902:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_hurricane);
                    break;
                case 903:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_snowflake_cold);
                    break;
                case 904:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_hot);
                    break;
                case 905:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_windy);
                    break;
                case 906:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_hail);
                    break;
                case 957:
                default:
                    icon = WeatherApplication.getAppContext().getString(R.string.weather_strong_wind);
                    break;
            }
        }

        return icon;
    }
}
