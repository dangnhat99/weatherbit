package cuoiki.nhom13.weatherforecast.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;

public class EditTextDialog {

    AlertDialog dialog;
    OnEditTextDialogListener editTextDialogListener;

    public void setOnEditTextDialogListener(OnEditTextDialogListener editTextDialogListener)  {
        this.editTextDialogListener = editTextDialogListener;
    }

    public EditTextDialog(Context context, String dialogMessage, String dialogTitle, String acceptTitle, String cancelTitle) {
        final EditText edittext = new EditText(context);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(dialogTitle);
        dialogBuilder.setMessage(dialogMessage);
        dialogBuilder.setView(edittext);
        dialogBuilder.setPositiveButton(acceptTitle, (dialogInterface, i) -> {
            if (editTextDialogListener != null) {
                editTextDialogListener.onAccept(edittext.getText().toString());
            }
        });

        dialogBuilder.setNegativeButton(cancelTitle, (dialogInterface, i) -> {
           if (editTextDialogListener != null ) {
               editTextDialogListener.onCancel();
           }
        });

        dialog = dialogBuilder.create();
    }

    public void show() {
        dialog.show();
    }
    public void dismiss() {dialog.dismiss();}

    public interface OnEditTextDialogListener {
        void onAccept(String editText);
        void onCancel();
    }

}
