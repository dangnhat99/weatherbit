//  Created by Dmnhat on 04/10/2020

package cuoiki.nhom13.weatherforecast.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initListener();
    }

    protected abstract void initView();

    protected abstract void initListener();
}
