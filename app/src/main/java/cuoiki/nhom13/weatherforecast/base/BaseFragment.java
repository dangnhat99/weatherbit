//  Created by Dmnhat on 04/10/2020

package cuoiki.nhom13.weatherforecast.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import cuoiki.nhom13.weatherforecast.ui.main.MainFragment;

public abstract class BaseFragment extends Fragment {
    protected NavController navController;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        initView();
        initListener();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected boolean changeFragment(int actionId) {
        try {
            MainFragment.isSwitchFragment = true;
            navController.navigate(actionId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean backFragment() {
        try {
            navController.popBackStack();
                    return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected abstract void initView();

    protected abstract void initListener();
}
