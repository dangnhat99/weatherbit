
package cuoiki.nhom13.weatherforecast.data.model.currentweather;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class CurrentWeatherModel extends RealmObject {

    @SerializedName("data")
    @Expose
    private RealmList<CurrentDatum> data = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public RealmList<CurrentDatum> getData() {
        return data;
    }

    public void setData(RealmList<CurrentDatum> data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
