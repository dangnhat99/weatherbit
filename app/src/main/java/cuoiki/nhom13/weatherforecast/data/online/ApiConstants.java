package cuoiki.nhom13.weatherforecast.data.online;

public class ApiConstants {
    public final static String API_KEY = "5ba877c3bd3e47d192cae9420df4e632";
    //public final static String API_KEY = "f3897477d7e048918059965407d46ac4";

    public final static String BASE_URL = "https://api.weatherbit.io/v2.0/";
    public final static String GET_CURRENT_WEATHER = "current";
    public final static String GET_TOMORROW_WEATHER = "forecast/hourly";
    public final static String GET_16DAYS_WEATHER = "forecast/daily";

    public final static String QUERY_KEY = "key";
    public final static String QUERY_CITY = "city";
    public final static String QUERY_LAT = "lat";
    public final static String QUERY_LON = "lon";
}
