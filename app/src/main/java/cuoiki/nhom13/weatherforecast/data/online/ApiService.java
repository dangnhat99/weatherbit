package cuoiki.nhom13.weatherforecast.data.online;

import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//singleton class
public class ApiService {
    private static ApiService instance = null;

    // Keep your services here, build them in buildRetrofit method later
    private WeatherApiService service;

    public static ApiService getInstance() {
        if (instance == null) {
            instance = new ApiService();
        }

        return instance;
    }

    // Build retrofit once when creating a single instance
    private ApiService() {
        // Implement a method to build your retrofit
        buildRetrofit();
    }

    private void buildRetrofit() {
        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(client)
                .build();

        // Build your services once
        this.service = retrofit.create(WeatherApiService.class);
    }

    public WeatherApiService getService() {
        return this.service;
    }
}
