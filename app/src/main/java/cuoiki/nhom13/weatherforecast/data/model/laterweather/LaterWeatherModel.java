
package cuoiki.nhom13.weatherforecast.data.model.laterweather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class LaterWeatherModel extends RealmObject {

    @SerializedName("data")
    @Expose
    private RealmList<LaterDatum> data = null;
    @SerializedName("count")
    @Expose
    private Integer count;

    public List<LaterDatum> getData() {
        return data;
    }

    public void setData(RealmList<LaterDatum> data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
