package cuoiki.nhom13.weatherforecast.data.offline.pref;

public class PrefDefault {
    public final static double CITY_LAT = 16.06778;
    public final static double CITY_LON = 108.22083;
    public final static boolean GRAPH_STATE = false;
}
