package cuoiki.nhom13.weatherforecast.data.offline.pref;

public class PrefKey {
    public static String CITY_LAT = "CITY_LAT";
    public static String CITY_LON = "CITY_LON";
    public static String LAST_UPDATE = "LAST_UPDATE";
    public static String IS_GRAPH_16DAYS = "IS_GRAPH_16DAYS";
}
