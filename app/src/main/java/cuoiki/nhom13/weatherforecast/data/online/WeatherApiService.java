package cuoiki.nhom13.weatherforecast.data.online;

import cuoiki.nhom13.weatherforecast.data.model.currentweather.CurrentWeatherModel;
import cuoiki.nhom13.weatherforecast.data.model.laterweather.LaterWeatherModel;
import cuoiki.nhom13.weatherforecast.data.model.tomorrowweather.TomorrowWeatherModel;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApiService {
    @GET(ApiConstants.GET_CURRENT_WEATHER)
    Single<CurrentWeatherModel> getCurrentWeatherByCity(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_CITY) String city
    );

    @GET(ApiConstants.GET_TOMORROW_WEATHER)
    Single<TomorrowWeatherModel> getTomorrowWeatherByCity(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_CITY) String city
    );

    @GET(ApiConstants.GET_CURRENT_WEATHER)
    Single<CurrentWeatherModel> getCurrentWeatherByLatLon(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_LAT) Double lat,
            @Query(ApiConstants.QUERY_LON) Double lon
    );
    @GET(ApiConstants.GET_TOMORROW_WEATHER)
    Single<TomorrowWeatherModel> getTomorrowWeatherByLatLon(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_LAT) Double lat,
            @Query(ApiConstants.QUERY_LON) Double lon
    );

    @GET(ApiConstants.GET_16DAYS_WEATHER)
    Single<LaterWeatherModel> getLaterWeatherByCity(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_CITY) String city
    );

    @GET(ApiConstants.GET_16DAYS_WEATHER)
    Single<LaterWeatherModel> getLaterWeatherByLatLon(
            @Query(ApiConstants.QUERY_KEY) String apiKey,
            @Query(ApiConstants.QUERY_LAT) Double lat,
            @Query(ApiConstants.QUERY_LON) Double lon
    );
}
